*OUTDATED* This repository is outdated and i will not update it any more. Please use [this newer version](https://github.com/heronils/Code_Browser_49).

# CB User Script
Collection of [Code Browser][cb] [user scripts][cb doc scr] providing the following functions:

* Quick [Folding][CB doc fold] of code blocks
* Navigating between code blocks
* Recursively outcommenting code
* Moving selected code

[Download][down ucbs]

The [wiki] contains examples, installing instructions and glossary.

License is [GPL 3.0][gpl].

Author: [Nils-Hero Lindemann] (I am not related to [Marc Kerbiquet][kerb], the creator of [Code Browser][cb]).

[ex fold]: #markdown-header-folding-blocks "Examples -> folding blocks"
[ex fold quick]: #markdown-header-quick-folding "Examples -> folding blocks -> quickfolding"
[ex fold nam]: #markdown-header-folding-named-blocks "Examples -> folding blocks -> folding named blocks"
[ex nav]: #markdown-header-block-navigation "Examples -> block navigation"
[ex outc]: #markdown-header-outcommenting-code "Examples -> outcommenting code"
[ex move]: #markdown-header-moving-selection "Examples -> moving selection"
[inst]: #markdown-header-installation "Installation"
[glos com]: #markdown-header-comment-types "Glossary -> comment types"
[glos block]: #markdown-header-block "Glossary -> block"
[glos block quick]: #markdown-header-quickfold "Glossary -> block -> quickfold"
[glos namblock]: #markdown-header-named-block "Glossary -> named block"
[glos namblock sep]: #markdown-header-block-separators "Glossary -> named block -> block separators"
[glos namblock val]: #markdown-header-valid-named-blocks "Glossary -> named block -> valid named blocks"
[whatgood]: #markdown-header-what-is-this-good-for "What is this good for?"

[Nils-Hero Lindemann]: mailto:nilsherolindemann@gmail.com "nilsherolindemann@gmail.com"
[download]: https://bitbucket.org/nilshero/cb-user-script/downloads "CB User Script -> downloads"
[overview]: https://bitbucket.org/nilshero/cb-user-script/overview "CB User Script -> Overview"
[src]: https://bitbucket.org/nilshero/cb-user-script/src "CB User Script -> Sources"
[wiki]: https://bitbucket.org/nilshero/cb-user-script/wiki/Home "CB User Script -> wiki"
[down ucbs]: https://bitbucket.org/nilshero/cb-user-script/downloads/user.cbs "Download the user.cbs"

[cb]: http://tibleiz.net/code-browser/index.html "Code Browser (external link)"
[cb doc files]: http://tibleiz.net/code-browser/doc-customizing-files.html "Code Browser -> doc -> files (external link)"
[cb doc fold]: http://tibleiz.net/code-browser/doc-folding.html "Code Browser -> doc -> folding (external link)"
[cb doc scr]: http://tibleiz.net/code-browser/doc-script.html "Code Browser -> doc -> scripting (external link)"
[cb doc scr class b]: http://tibleiz.net/code-browser/doc-script-classes-textblock.html "Code Browser -> doc -> scripting ->  class reference -> model -> TextBlock"
[cb doc scr quick]: http://tibleiz.net/code-browser/doc-script-quick-start.html "Code Browser -> doc -> scripting -> quickstart (external link)"
[cb feat]: http://tibleiz.net/code-browser/features.html "Code Browser -> features (external link)"
[gpl]: http://www.gnu.org/licenses/gpl-3.0.txt "GNU General Public License version 3.0 (external link)"
[kerb]: http://tibleiz.net/ "Marc Kerbiquet�s homepage (external link)"

